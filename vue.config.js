module.exports = {
  publicPath: "./",
  productionSourceMap: false,
  configureWebpack: {
    // provide the app's title in webpack's name field, so that
    // it can be accessed in index.html to inject the correct title.
    name: "汇报-友邦增员系统"
  },
  devServer: {
    open: true,
    port: 8888,
    proxy: {
      "/api": {
        target: "http://localhost:8888",
        ws: true,
        changeOrigin: true,
        pathRewrite: { "^/api": "" }
      }
    },
    overlay: {
      warnings: false,
      errors: false
    }
  },
  runtimeCompiler: true
};
