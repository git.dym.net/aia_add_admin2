import Vue from 'vue'
import App from './App.vue'
import router from "./router";
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import 'vant/lib/index.css';
import '@/style/common.css';
import Vant from 'vant';
import Vconsole from 'vconsole';
const vConsole = new Vconsole()
Vue.use(vConsole);
Vue.use(Vant);
Vue.config.productionTip = false
Vue.use(ElementUI);
new Vue({
  router,
  render: h => h(App),
}).$mount('#app')