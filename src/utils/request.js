import Vue from "vue";
import axios from "axios";
import {
  Toast
} from "vant";
Vue.prototype.$axios = axios;

// 创建axios实例
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // api的base_url
  timeout: 150000000 // 请求超时时间
});

// request拦截器
service.interceptors.request.use(
  config => {
    return config;
  },
  error => {
    Promise.reject(error);
  }
);

// respone拦截器
service.interceptors.response.use(
  response => {
    console.log(response.data,'2')
    const res = response.data;
    if (res.status != 200) {
      if (res.status == 404) {
        Toast("找不到数据");
      } else if (res.status == 500) {
        Toast("服务器内部错误");
      } else if (res.status == 401) {
        Toast("未经授权");
        window.primeton.sendRnEvent({
          eventName: "_login_",
          params: {
            path: window.location.host+'/#/index'
          }
        });
        window.primeton.closeWindow();
      }else{
        Toast(res.message);
      }
      return Promise.reject(res);
    } else {
      return res;
    }
  },
  error => {
    if (error.response.status == 401) {
      Toast("未经授权");
    } else if (error.response.status == 500) {
      Toast("服务器内部错误");
    } else if (error.response.status == 404) {
      Toast("找不到数据");
    } else {
      Toast("网络错误");
    }

    return Promise.reject(error);
  }
);

export default service;