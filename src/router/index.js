import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [{
    path: "/",
    redirect: "/index"
  },
  {
    path: "/index",
    name: "Home",
    meta:{
      title:'友邦保险'
    },
    component: () => import("@/views/home/index")
  },
  {
    path: "/candidate",
    name: "candidate",
    component: () => import("@/components/candidate")
  },
  {
    path: "/rangeArea",
    name: "rangeArea",
    component: () => import("@/components/rangeArea")
  },
  {
    path: "/talentpool",
    name: "TalentPool",
    meta:{
      title:'人才库'
    },
    component: () => import("@/views/talentpool/index")
  },
  {
    path: "/addProspectiveStaff",
    name: "AddProspectiveStaff",
    meta: {
      title:'增员邀约'
    },
    component: () => import("@/views/addProspectiveStaff/index")
  }, {
    path: "/evaluation",
    name: "Evaluation",
    meta: {
      title:'天生赢家'
    },
    component: () => import("@/views/evaluation/index")
  },
  // {
  //   path: "/question",
  //   name: "Question",
  //   component: () => import("@/views/evaluation/question")
  // },
  // {
  //   path: "/moreQuestion",
  //   name: "MoreQuestion",
  //   component: () => import("@/views/evaluation/moreQuestion")
  // },
  // {
  //   path: "/chart",
  //   name: "Chart",
  //   component: () => import("@/views/evaluation/chart")
  // },
  // {
  //   path: "/conclusion",
  //   name: "Conclusion",
  //   component: () => import("@/views/evaluation/conclusion")
  // },
  // {
  //   path: "/interview",
  //   name: "Interview",
  //   component: () => import("@/views/evaluation/interview")
  // }
];
const router = new VueRouter({
  base: process.env.BASE_URL,
  routes
});

export default router;